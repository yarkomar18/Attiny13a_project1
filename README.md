# Wireless thermometer 

The tiny wireless thermometer module, which can connect to the smart home system. 
Reading temperature within 0 - 112 *C
Update data every 150-300 ms

  - Attiny 13a 
  http://ww1.microchip.com/downloads/en/DeviceDoc/doc8126.pdf
  - DS18b20
  https://datasheets.maximintegrated.com/en/ds/DS18B20.pdf
  - NRF24
  https://www.sparkfun.com/datasheets/Components/SMD/nRF24L01Pluss_Preliminary_Product_Specification_v1_0.pdf

# NRF24 parameters for reading
    RF24 radio(ce, csn);
    radio.setChannel      (10);
    radio.setCRCLength(RF24_CRC_16);
    radio.setDataRate     (RF24_1MBPS);                  
    radio.setPALevel      (RF24_PA_MAX);  
    radio.openReadingPipe (1, 0xc2c2c2c2c2LL);             
    // ...
    int data;
    radio.read( &data, sizeof(data) );             


# Attiny pinout
![attiny13 pinout](http://robocraft.ru/files/arduino_programmer/attiny13-pinout.jpg)


# NFR24 pinout
![NRF24 pinout](https://components101.com/sites/default/files/component_pin/nRF24L01-Pinout.png)

# DS18B20 pinout
![DS18B20](https://os.mbed.com/media/components/pinouts/DS18B20.jpg)


# Circuit board
![enter image description here](https://lh3.googleusercontent.com/rj5ZCh22sVSG38lDJj44xGwfQJinE1s0rHD_8LIHlicOx6EDAe7L4L07h1R98BmMZzdOyzxLesLY "term circuit")

