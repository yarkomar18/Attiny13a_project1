/*
 * Attiny13a_project1.cpp
 *
 * Created: 03.02.2018 17:23:21
 * Author : Yaroslav
 */ 

// 1.2 MHz (default) built in resonator
#define F_CPU 1200000UL

#include <avr/io.h>
#include <util/delay.h>


// ---------------- PORTS ----------------
		  // temperature sensor
#define DS _BV(PB0); 
				// nrf24
#define READ_PIN _BV(PB0);
#define WRITE_PIN _BV(PB1);
#define SCK_PIN _BV(PB2);
#define CE_PIN _BV(PB3);
#define CSN_PIN _BV(PB4);


// -------------- CONSTANTS ----------------
			// temperature sensor
#define DS_SKIP_ROM 0xCC
#define DS_CONVERT 0x44
#define DS_READ 0xBE
				// nrf24
/* Memory Map */
#define NRF_CONFIG  0x00 ////// NRF_CONFIG 
#define EN_AA       0x01
#define EN_RXADDR   0x02
#define SETUP_AW    0x03
#define SETUP_RETR  0x04
#define RF_CH       0x05
#define RF_SETUP    0x06
#define NRF_STATUS  0x07
#define OBSERVE_TX  0x08
#define CD          0x09
#define RX_ADDR_P0  0x0A // +
#define RX_ADDR_P1  0x0B
#define RX_ADDR_P2  0x0C
#define RX_ADDR_P3  0x0D
#define RX_ADDR_P4  0x0E
#define RX_ADDR_P5  0x0F
#define TX_ADDR     0x10 // +
#define RX_PW_P0    0x11 // +
#define RX_PW_P1    0x12
#define RX_PW_P2    0x13
#define RX_PW_P3    0x14
#define RX_PW_P4    0x15
#define RX_PW_P5    0x16
#define FIFO_STATUS 0x17
#define DYNPD	    0x1C
#define FEATURE	    0x1D

/* Bit Mnemonics */
#define EN_CRC      (1 << 3)
#define CRCO        (1 << 2)
#define PWR_UP      (1 << 1) 
#define PRIM_RX     (1 << 0)
#define RF_PWR      6 // +


/* Instruction Mnemonics */
#define R_REGISTER    0x00
#define W_REGISTER    0x20 // +
#define REGISTER_MASK 0x1F
#define ACTIVATE      0x50
#define R_RX_PL_WID   0x60
#define R_RX_PAYLOAD  0x61
#define W_TX_PAYLOAD  0xA0 // +
#define W_ACK_PAYLOAD 0xA8
#define FLUSH_TX      0xE1 // +
#define FLUSH_RX      0xE2 // +
#define REUSE_TX_PL   0xE3
#define RF24_NOP      0xFF

/* Non-P omissions */
#define LNA_HCURR   0

/* P model memory Map */
#define RPD         0x09
#define W_TX_PAYLOAD_NO_ACK  0xB0

/* P model bit Mnemonics */
#define RF_DR_LOW   5
#define RF_DR_HIGH  3
#define RF_PWR_LOW  1
#define RF_PWR_HIGH 2


//----------------------------------------------------------------------------------------
/*
				$$$$$$__$$__$$__$$__$$___$$$$___$$$$$$__$$$$$$___$$$$___$$__$$___$$$$
				$$______$$__$$__$$$_$$__$$__$$____$$______$$____$$__$$__$$$_$$__$$
				$$$$____$$__$$__$$_$$$__$$________$$______$$____$$__$$__$$_$$$___$$$$
				$$______$$__$$__$$__$$__$$__$$____$$______$$____$$__$$__$$__$$______$$
				$$_______$$$$___$$__$$___$$$$_____$$____$$$$$$___$$$$___$$__$$___$$$$
*/
//----------------------------------------------------------------------------------------

bool ds_init();
void ds_send(unsigned char _rom);
bool  ds_read();
int ds_getTemperature();

void nrf_init();
void startWrite();
void stopWrite();
inline void csn_event();
void nrf_write_byte(uint8_t _data);
//uint8_t nrf_read_byte(); // not used

//----------------------------------------------------------------------------------------
/*
							$$___$$__$$$$___$$$$$$__$$__$$
							$$$_$$$_$$__$$____$$____$$$_$$
							$$_$_$$_$$$$$$____$$____$$_$$$
							$$___$$_$$__$$____$$____$$__$$
							$$___$$_$$__$$__$$$$$$__$$__$$
*/
//----------------------------------------------------------------------------------------

// 2 = int size (2 bytes)
#define dataSize 2
int temperature = 0;


int main()
{
    // initial condition of registers
	DDRB |= CSN_PIN;
	DDRB |= WRITE_PIN;
	DDRB &= ~READ_PIN;
	DDRB |= SCK_PIN;
	DDRB |= CE_PIN;

	PORTB |= CSN_PIN; // HIGH by default
	PORTB &= ~SCK_PIN; // LOW by default
	PORTB &= ~CE_PIN; // LOW by default

    // delays were taken from datasheets
    // but in some cases, a logic analyzer was used
    
	_delay_ms(1000);
	nrf_init(); //
	_delay_ms(1000);

	while (1)
	{
	    // get temperature
		temperature = ds_getTemperature();
		
		// send data
		//  12345 (2 )
		startWrite();
		stopWrite();
		_delay_ms(100);
	}
}




//----------------------------------------------------------------------------------------
/*
							$$__$$__$$$$$___$$$$$$___$$$$___$$
							$$$_$$__$$__$$__$$______$$__$$__$$__$$
							$$_$$$__$$$$$___$$$$_______$$___$$$$$$
							$$__$$__$$__$$__$$_______$$_________$$
							$$__$$__$$__$$__$$______$$$$$$______$$
*/
//----------------------------------------------------------------------------------------

void nrf_init(){
	uint8_t senddata = 0;
	
	_delay_us(20);
	PORTB &= ~CE_PIN; // CE low
	_delay_us(20);
	PORTB &= ~CSN_PIN; // CSN low
	_delay_us(20);

/*
  SETUP_RETR
   1750 
  15
ARD=0101 ARC=1111
*/


	nrf_write_byte(W_REGISTER | SETUP_RETR);
//		nrf_write_byte(0x5F);
		nrf_write_byte(0x22);



	csn_event();

/*
  RF_SETUP
   RF 0 dBm   TX
*/


	nrf_write_byte(W_REGISTER | RF_SETUP);
		nrf_write_byte(0x06);



	csn_event();
	
	

/*
  NRF_CONFIG
EN_CRC  
CRCO   2 
PRIM_RX    0 - 
*/
	nrf_write_byte(W_REGISTER | NRF_CONFIG);
		senddata = 0;
		senddata |= EN_CRC;
		senddata |= CRCO; 
		nrf_write_byte(senddata);  //send data (0b00001100)



	csn_event();
	
	
	
/*
   RF_CH
  10 + 2400
10 
*/
//
// chanel
	nrf_write_byte(W_REGISTER | RF_CH);
		nrf_write_byte(10);
	
	
	
	csn_event();
	
	
	
/*
   RX_PW_P0
  RX   
*/
	nrf_write_byte(W_REGISTER | RX_PW_P0); // 0x11
		nrf_write_byte(2); // 2 FIXME
	
	
	
	csn_event();
	
	
	
/*
 TX FIFO
*/
	nrf_write_byte(FLUSH_TX); // 0xE1
		nrf_write_byte(0);
	
	
	
	csn_event();
	
	
	
/*
 RX FIFO
*/

	nrf_write_byte(FLUSH_RX); // 0xE2
		nrf_write_byte(0);
		
		
	
	csn_event();

/*
  rx   
const uint64_t pipe = 0xFF000000FFLL;
*/	
// pipe
	nrf_write_byte(W_REGISTER | RX_ADDR_P0); //0x0A
  		nrf_write_byte(0xc2);
  		nrf_write_byte(0xc2);
  		nrf_write_byte(0xc2);
		nrf_write_byte(0xc2);
  		nrf_write_byte(0xc2);
  	
	csn_event();
	
/*
  . LSB  
const uint64_t pipe = 0xFF000000FFLL;
*/

// pipe
	nrf_write_byte(W_REGISTER | TX_ADDR); //0x10
  		nrf_write_byte(0xc2);
  		nrf_write_byte(0xc2);
  		nrf_write_byte(0xc2);
  		nrf_write_byte(0xc2);
  		nrf_write_byte(0xc2);

	_delay_us(20);
	PORTB |= CSN_PIN; // CSN HIGH
	//_delay_us(20);	
	//PORTB |= CE_PIN; // enable chip, allow transreceiving
}

void startWrite(){
	_delay_us(20);
	
	PORTB &= ~CE_PIN; // CE low
	_delay_us(20);
	PORTB &= ~CSN_PIN; // CSN low
    _delay_us(20);
	
    // writing data to register
	nrf_write_byte(W_REGISTER | NRF_CONFIG);
		uint8_t senddata = 0;
		senddata |= EN_CRC;
		senddata |= CRCO;
		senddata |= PWR_UP;
		senddata &= ~PRIM_RX;
		nrf_write_byte(senddata);  //send data (0b00001110)
		
		_delay_us(20);
	
	PORTB |= CSN_PIN; // CSN high (inactive)
	
	_delay_us(1500);
	
/*
 12345
2 
*/
	PORTB &= ~CSN_PIN; // CSN low
	_delay_us(20);

	nrf_write_byte(W_TX_PAYLOAD); //0xA0
		nrf_write_byte(temperature);
		nrf_write_byte(temperature >> 8);
	
//		nrf_write_byte(0b00111001);
//		nrf_write_byte(0b00110000);
		
	_delay_us(20);
	PORTB |= CSN_PIN; // CSN HIGH
	
	_delay_us(20);
	PORTB |= CE_PIN; // CE high
	_delay_us(200);
	PORTB &= ~CE_PIN; // CE low
}

void stopWrite(){
	_delay_us(20);
	PORTB &= ~CSN_PIN; // CSN low
	
	_delay_us(20);

	nrf_write_byte(W_REGISTER | NRF_CONFIG);
		uint8_t senddata = 0;
		senddata |= EN_CRC;
		senddata |= CRCO;
		senddata &= ~PWR_UP;
		senddata &= ~PRIM_RX;
		nrf_write_byte(senddata);  //send data (0b00001100)
	
	_delay_us(20);
	PORTB |= CSN_PIN; // CSN HIGH
}

inline void csn_event(){
		_delay_us(100);
		PORTB |= CSN_PIN; // CSN HIGH
		_delay_us(100);
		PORTB &= ~CSN_PIN; // CSN low
		_delay_us(100);
}

// --------------

// read function not used
/*
uint8_t nrf_read_byte(){//uint8_t _data){
	uint8_t _getdata = 0;
	for (int i = 7; i >= 0; i--)
	{
		_delay_us(20);
		
		if (PINB&(1<<PINB0)) // if '1'
		{
			_getdata |= (1 << i);
		}  
		else // if '0'
		{
			_getdata &= ~(1 << i);
		}
			
		_delay_us(20);
		PORTB |= SCK_PIN;
		_delay_us(40);
		PORTB &= ~SCK_PIN;
	
	}
	
	return _getdata;
}
*/

// reading byte from nrf24
void nrf_write_byte(uint8_t _data){
	for (int i = 7; i >= 0; --i) {
		_delay_us(20);
		
		if ((_data & (1 << i)) == (1 << i)) {
			PORTB |= WRITE_PIN;
		}
		else {
			PORTB &= ~WRITE_PIN;
		}
		
		_delay_us(20);
		PORTB |= SCK_PIN;
		_delay_us(40);
		PORTB &= ~SCK_PIN;
		
	}
}

//----------------------------------------------------------------------------------------
/*
								$$$$$____$$$$______$$____$$$$
								$$__$$__$$_______$$$$___$$__$$
								$$__$$___$$$$______$$____$$$$
								$$__$$______$$_____$$___$$__$$
								$$$$$____$$$$______$$____$$$$
*/
//----------------------------------------------------------------------------------------

// initialization ds18b20 
bool ds_init(){
	DDRB &= ~DS; // port pb3 input
	if(PINB & (1<<PINB0)){
		DDRB |= DS; // port pb3 output
		PORTB &= ~DS; // low
		_delay_us(500);
		DDRB &= ~DS; // port pb3 input + 12 us
		_delay_us(60);
		if(!(PINB & (1<<PINB0))){
			_delay_us(500);
			if(PINB & (1<<PINB0)){
				return true;
			}
		}
	}	
return false;
}

// send one byte to ds18b20
void ds_send(unsigned char _rom){
	for (unsigned char i = 0; i < 8; i++)
	{
		if ((_rom & (1<<i)) == 1<<i) // if '1'
		{
			DDRB |= DS; // port pb3 output
			PORTB &= ~DS; // low
			_delay_us(2);
			PORTB |= DS; // HIGH	
			_delay_us(65);
		} 
		else // if '0'
		{
			DDRB |= DS; // port pb3 output
			PORTB &= ~DS; //LOW
			_delay_us(65);
			PORTB |= DS; // HIGH
			_delay_us(2);
		}
	}
}

// reading one byte from ds18b20
bool  ds_read(){
		DDRB |= DS; // port pb3 output
		PORTB &= ~DS; // low
		_delay_us(2);
		PORTB |= DS; // high	
		
		DDRB &= ~DS; // port pb3 input
		_delay_us(13);
		if (PINB&(1<<PINB0)) // if '1'
		{
			return 1;
		}
		else // if '0' 
		{
			return 0;
		}
		_delay_us(50);
}

// reading temperature from ds18b20
int ds_getTemperature() {
	int temp = -5;
	
	if(ds_init()){
		ds_send(DS_SKIP_ROM);
		ds_send(DS_CONVERT);
		
		_delay_ms(750);
		
		if(ds_init()){
			ds_send(DS_SKIP_ROM);
			ds_send(DS_READ);
			
			for (int i = 0; i < 16; i++)
			{
				if(ds_read()){
					temp |= (1<<i);
				}
				else{
					temp &= ~(1<<i);
				}
			}
			// division by 16
			// separate the fractional part
			temp = temp >> 4;	
		}
		
		// for debugging 
		if(temp > 125 || temp < 0){
			return -6;
		}
	}
	
	return temp;
}

//----------------------------------------------------------------------------------------
